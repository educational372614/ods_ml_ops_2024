В проекте в качестве линтера и форматера используется **ruff**, общие настройки лежат в <em>pyproject.toml</em>.<br>
Для использования в **VS Code** нужно скачать расширение и записать следующие настройки в <em>.vscode/settings.json</em>:
~~~
        {
            "notebook.formatOnSave.enabled": true,
            "notebook.codeActionsOnSave": {
                "notebook.source.fixAll": "explicit",
                "notebook.source.organizeImports": "explicit"
            },
            "[python]": {
                "editor.formatOnSave": true,
                "editor.defaultFormatter": "charliermarsh.ruff",
                "editor.codeActionsOnSave": {
                    "source.fixAll": "explicit",
                    "source.organizeImports": "explicit"
                }
            }
        }
~~~
